package com.nextdots.iteractive.di;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.nextdots.iteractive.di.components.ApplicationComponent;
import com.nextdots.iteractive.di.components.DaggerApplicationComponent;
import com.nextdots.iteractive.di.modules.ApplicationModule;



public class App extends Application {

    public ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
    }

    protected void initializeInjector(){
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
