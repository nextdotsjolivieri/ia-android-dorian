package com.nextdots.iteractive.di.modules;

import android.app.Application;
import android.content.Context;

import com.nextdots.iteractive.data.db.DBOperations;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }


    @Provides
    @Singleton
    DBOperations provideDBOperations() {
        return new DBOperations(application);
    }

}
