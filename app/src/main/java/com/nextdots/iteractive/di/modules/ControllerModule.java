package com.nextdots.iteractive.di.modules;


import com.nextdots.iteractive.api.controllers.CityController;
import com.nextdots.iteractive.api.services.CityAPi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ControllerModule {


    @Provides
    @Singleton
    CityController cityController(CityAPi cityAPi) {
        return new CityController(cityAPi);
    }
}
