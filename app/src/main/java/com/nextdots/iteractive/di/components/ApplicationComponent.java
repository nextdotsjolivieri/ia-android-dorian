package com.nextdots.iteractive.di.components;

import com.nextdots.iteractive.ui.modules.detailCity.DetailCityFragment;
import com.nextdots.iteractive.ui.modules.detailComplejo.DetailComplejoFragment;
import com.nextdots.iteractive.ui.modules.main.MainActivity;
import com.nextdots.iteractive.di.modules.ApiModule;
import com.nextdots.iteractive.di.modules.ApplicationModule;
import com.nextdots.iteractive.di.modules.ControllerModule;
import com.nextdots.iteractive.ui.base.BaseActivity;
import com.nextdots.iteractive.ui.base.BaseFragment;
import com.nextdots.iteractive.ui.modules.main.MainFragment;
import com.nextdots.iteractive.ui.modules.splash.SplashFragment;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                ApiModule.class,
                ControllerModule.class
        }
)

public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);

    void inject(MainActivity mainActivity);

    void inject(SplashFragment splashFragment);

    void inject(MainFragment mainFragment);

    void inject(DetailCityFragment detailCityFragment);

    void inject(DetailComplejoFragment detailComplejoFragment);
}
