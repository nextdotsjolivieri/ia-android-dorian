package com.nextdots.iteractive.ui.modules.galery;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.ui.adapters.GaleryAdapter;
import com.nextdots.iteractive.ui.base.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link GaleryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GaleryFragment extends BaseFragment {
    private static final String ARG_FUNCION = "funcion";

    private GaleryAdapter mAdapter;
    private GridLayoutManager mLayoutManager;
    private Funcion funcion;

    @BindView(R.id.llWithoutElement)
    LinearLayout llWithoutElement;
    @BindView(R.id.rvGalery)
    RecyclerView rvGalery;

    public GaleryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param funcion Parameter 1.
     * @return A new instance of fragment GaleryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GaleryFragment newInstance(Funcion funcion) {
        GaleryFragment fragment = new GaleryFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_FUNCION, funcion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            funcion = getArguments().getParcelable(ARG_FUNCION);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_galery;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        mAdapter = new GaleryAdapter(getContext(), funcion.getMultimedias(), Glide.with(this));
        if(funcion.getMultimedias().isEmpty()){
            llWithoutElement.setVisibility(View.VISIBLE);
        }else{
            llWithoutElement.setVisibility(View.GONE);
        }

        mLayoutManager = new GridLayoutManager(getContext(),3);
        rvGalery.setLayoutManager(mLayoutManager);
        rvGalery.setAdapter(mAdapter);
    }

}
