package com.nextdots.iteractive.ui.modules.main;

import android.content.Context;
import android.os.Bundle;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.ui.base.BaseActivity;
import com.nextdots.iteractive.ui.modules.splash.SplashFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends BaseActivity implements MainActivityView {

    private boolean isVisibleMain = false;

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            pushFragment(SplashFragment.newInstance(), R.id.container, false);
        } else if (isVisibleMain) {
            pushFragment(MainFragment.newInstance(), R.id.container, false);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void gotoMain() {
        isVisibleMain = true;
        pushFragment(MainFragment.newInstance(), R.id.container, false);
    }
}
