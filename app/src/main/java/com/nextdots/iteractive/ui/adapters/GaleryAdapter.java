package com.nextdots.iteractive.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.RequestManager;
import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.Multimedia;
import com.nextdots.iteractive.utils.Constant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GaleryAdapter extends RecyclerView.Adapter<GaleryAdapter.GaleryViewHolder>{

    private final RequestManager glide;
    private List<Multimedia> list;
    private Context context;

    public static class GaleryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivImage)
        ImageView ivImage;

        GaleryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public GaleryAdapter(Context context, List<Multimedia> list, RequestManager glide){
        this.context = context;
        this.list = list;
        this.glide = glide;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public GaleryAdapter.GaleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_galery,parent,false);
        return new GaleryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GaleryViewHolder holder, int position) {
        Multimedia multimedia = list.get(position);
        glide.load(Constant.URL_IMAGE+multimedia.getArchivo())
                .error(R.drawable.ic_film)
                .dontAnimate()
                .placeholder(R.drawable.ic_film)
                .into(holder.ivImage);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public Multimedia getItemPosition(int position){
        return list.get(position);
    }


}