package com.nextdots.iteractive.ui.modules.main;


import com.nextdots.iteractive.api.controllers.CityController;
import com.nextdots.iteractive.data.db.DBOperations;
import com.nextdots.iteractive.data.models.City;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class MainFragmentPresenter {

    private final MainFragmentView mainFragmentView;
    private final DBOperations dbOperations;

    public MainFragmentPresenter(MainFragmentView mainFragmentView, DBOperations dbOperations) {
        this.mainFragmentView = mainFragmentView;
        this.dbOperations = dbOperations;
    }


    public void getCities(){
        dbOperations.getCities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess);
    }

    public void onSuccess(List<City> cities){
        mainFragmentView.setCitiesAdapter(cities);
    }

}
