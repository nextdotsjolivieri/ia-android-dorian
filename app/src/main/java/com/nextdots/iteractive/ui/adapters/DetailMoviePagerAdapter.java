package com.nextdots.iteractive.ui.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.ui.modules.galery.GaleryFragment;
import com.nextdots.iteractive.ui.modules.horarios.HorariosFragment;
import com.nextdots.iteractive.ui.modules.trailer.TrailerFragment;

import java.util.HashMap;
import java.util.Map;



public class DetailMoviePagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private Context context;
    private final City city;
    private Funcion funcion;
    private FragmentManager fm;
    private Complejo complejo;
    private Map<Integer, String> mFragmentTags;
    private int[] stringResId = {
        R.string.galery, R.string.trailer, R.string.horarios
    };


    public DetailMoviePagerAdapter(FragmentManager fm, Context context, City city, Complejo complejo,
                                   Funcion funcion) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.city = city;
        this.funcion = funcion;
        this.complejo = complejo;
        mFragmentTags = new HashMap<>();
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) {
            return GaleryFragment.newInstance(funcion);
        }else if( position == 1){
            return TrailerFragment.newInstance(funcion);
        }else{
            return HorariosFragment.newInstance(funcion);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getString(stringResId[position]);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object obj = super.instantiateItem(container, position);
        if (obj instanceof Fragment) {
            // record the fragment tag here.
            Fragment f = (Fragment) obj;
            String tag = f.getTag();
            mFragmentTags.put(position, tag);
        }
        return obj;
    }

    public Fragment getFragment(int position) {
        String tag = mFragmentTags.get(position);
        if (tag == null)
            return null;
        return fm.findFragmentByTag(tag);
    }


}
