package com.nextdots.iteractive.ui.base;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.PermissionResult;
import com.nextdots.iteractive.di.App;
import com.nextdots.iteractive.di.components.ApplicationComponent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;



public abstract class BaseActivity extends AppCompatActivity {
    public static final int TRANSACTION_WITHOUT_ANIMATION = 0;
    private final String TAG = BaseActivity.class.getSimpleName();


    protected FragmentManager fm;
    protected ArrayList<String> mTagFragments;
    private ProgressDialog progressDialog = null;
    private AlertDialog alertDialog = null;
    private boolean haveToolbar;
    private Toolbar mToolbar;

    @Nullable
    @BindView(R.id.tvTitle)
    TextView tvTitlte;

    abstract public int getLayout();

    abstract public void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Rubik-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        fm = getSupportFragmentManager();
        mTagFragments = new ArrayList<>();
        setContentView(getLayout());
        ButterKnife.bind(this);
        onCreateView(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void setToolbar(Toolbar toolBar) {
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            mToolbar = toolBar;
            haveToolbar = true;
        }
    }

    @Override
    public void setTitle(@NonNull @StringRes int titleId) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(titleId);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setSubtitle("");
            if(tvTitlte != null){
                tvTitlte.setText(title);
            }
        }
    }

    public void showToolbar() {
        if ((getSupportActionBar() != null) && haveToolbar) {
            setToolbar(mToolbar);
            mToolbar.setVisibility(View.VISIBLE);
        }
    }

    public void hideToolbar() {
        if ((getSupportActionBar() != null) && haveToolbar) {
            mToolbar.setVisibility(View.GONE);
        }
    }

    public void setupImageToolbar(@NonNull @DrawableRes int resImage, boolean enable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setHomeAsUpIndicator(resImage);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        }
    }

    public void setEnableBackToolbar(boolean enable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            getSupportActionBar().setDisplayShowHomeEnabled(enable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            if (mTagFragments.size() > 0) {
                Fragment fragment = fm.findFragmentByTag(mTagFragments.get(mTagFragments.size() - 1));
                if (fragment instanceof BaseFragment) {
                    BaseFragment base = (BaseFragment) fragment;
                    if (base.onBackToolbar()) {
                        return true;
                    }
                }
            }
            goBack();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (mTagFragments.size() > 0) {
            Fragment fragment = fm.findFragmentByTag(mTagFragments.get(mTagFragments.size() - 1));
            if (fragment instanceof BaseFragment) {
                BaseFragment base = (BaseFragment) fragment;
                if (base.onBackPressed()) {
                    return;
                }
            }
            mTagFragments.remove(mTagFragments.size() - 1);
        }
        super.onBackPressed();
    }

    public void goBack(int number) {
        if(number < 0){
            throw new RuntimeException("Error number back");
        }
        for (int i=0;i<number;i++){
            goBack();
        }
    }

    public void goBack() {
        if (mTagFragments.size() > 0) {
            mTagFragments.remove(mTagFragments.size() - 1);
            fm.popBackStackImmediate();
        }
    }

    public void pushFragment(Fragment fragment, int... animations) {
        pushFragment(fragment, R.id.container, true, animations);
    }

    public void pushFragment(Fragment fragment) {
        pushFragment(fragment, R.id.container, true);
    }

    public void pushFragment(Fragment fragment, int container, boolean addBackStack, int... animations) {
        FragmentTransaction transaction = fm.beginTransaction();
        String tag = fragment.getClass().getSimpleName();

        if (addBackStack) {
            transaction.addToBackStack(tag);
        }

        switch (animations.length) {
            case 0:
                transaction.setCustomAnimations(
                        R.anim.push_show_in_simple,
                        R.anim.push_hidden_out_simple,
                        0,
                        0);
                break;
            case 1:
                break;
            case 2:
                transaction.setCustomAnimations(animations[0], animations[1]);
                break;
            case 4:
                transaction.setCustomAnimations(animations[0], animations[1], animations[2], animations[3]);
                break;
            default:
                throw new RuntimeException("Error with animations transaction");
        }


        transaction.replace(container, fragment, tag);
        try {
            transaction.commit();
        } catch (Exception e) {
            return;
        }
        if((mTagFragments.size() == 0 )|| addBackStack){
            mTagFragments.add(tag);
        }
    }

    public void showFragment(Fragment fragment, String tag, boolean addBackStack) {
        FragmentTransaction transaction = fm.beginTransaction();

        if(addBackStack){
            transaction.addToBackStack(tag);
        }
        transaction.add(fragment, tag);
        transaction.commitAllowingStateLoss();
        if((mTagFragments.size() == 0 )|| addBackStack){
            mTagFragments.add(tag);
        }
    }

    public void showProgressDialog() {
        showProgressDialog(R.string.loading);
    }

    public void showProgressDialog(@NonNull @StringRes int resMsg) {
        showProgressDialog(getString(R.string.app_name), getString(resMsg), false);
    }


    public void showProgressDialog(String msg) {
        showProgressDialog(getString(R.string.app_name), msg, false);
    }

    public void showProgressDialog(@NonNull @StringRes int resMsg, boolean cancelable) {
        showProgressDialog(getString(R.string.app_name), getString(resMsg), cancelable);
    }

    public void showProgressDialog(String msg, boolean cancelable) {
        showProgressDialog(getString(R.string.app_name), msg, cancelable);
    }

    public void showProgressDialog(String title, String msg, boolean cancelable) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this,R.style.MyDialogTheme);
            progressDialog.setProgressStyle(R.style.MyDialogTheme);
            progressDialog.setCancelable(cancelable);
            progressDialog.setTitle(title);
            progressDialog.setMessage(msg);
            progressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void simpleToast(@NonNull @StringRes int resMsg) {
        Toast.makeText(this, resMsg, Toast.LENGTH_SHORT).show();
    }

    public void simpleToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    protected boolean isHorizontal() {
        boolean isHorizontal = false;
        int rotation = getWindowManager().getDefaultDisplay().getRotation();

        if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
            isHorizontal = true;
        }

        return isHorizontal;
    }


    public void rotate() {
        if (isHorizontal()) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        } else {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }
        }
        Log.d(TAG, "rotate");
    }

    public String getLastTagFragment() {
        if (mTagFragments.size() == 0) {
            return "";
        } else {
            return mTagFragments.get(mTagFragments.size() - 1);
        }
    }

    public BaseFragment findFragmentByTag(String tag){
        try{
            return (BaseFragment) fm.findFragmentByTag(tag);
        }catch (Exception e){
            return null;
        }
    }

    public void showOptionDialog(@NonNull @StringRes int resTitle , @NonNull @ArrayRes int resArray,
                                 DialogInterface.OnClickListener listener) {
        final CharSequence[] items = getResources().getStringArray(resArray);
        showOptionDialog(getResources().getString(resTitle), items, listener);
    }

    public void showOptionDialog(String title , CharSequence[] items, final DialogInterface.OnClickListener listener) {
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            builder.setTitle(title);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    listener.onClick(dialogInterface, i);
                    dialogInterface.dismiss();
                    alertDialog = null;
                }
            });
            alertDialog = builder.create();
            alertDialog.show();
        } catch(Exception e){
            Log.d(TAG+" Dars","error dialogue option");
        }
    }

    public ApplicationComponent getApplicationComponent() {
        return ((App) getApplication()).getApplicationComponent();
    }


    //----------------------------------------------------------------------------------------------
    // Permission Methods
    //----------------------------------------------------------------------------------------------

    public boolean hasPermissions(String... permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public boolean hasPermissionsOrRequest(@NonNull int requestCode, String... permissions) {
        List<String> denied = new ArrayList<>();

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                denied.add(permission);
            }
        }

        if (denied.size() > 0) {
            ActivityCompat.requestPermissions(this,
                    denied.toArray(new String[denied.size()]),
                    requestCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        onRequestPermissionsResult(PermissionResult.getResult(this,
                requestCode, permissions, grantResults));
    }

    public void onRequestPermissionsResult(PermissionResult result) {

    }
}
