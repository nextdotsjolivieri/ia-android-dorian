package com.nextdots.iteractive.ui.modules.detailComplejo;

import com.nextdots.iteractive.data.models.Funcion;

import java.util.ArrayList;

public interface DetailComplejoView {
    void setupMoviesAdapter(ArrayList<Funcion> movies);
}
