package com.nextdots.iteractive.ui.modules.horarios;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.ui.adapters.HorariosAdapter;
import com.nextdots.iteractive.ui.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HorariosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HorariosFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_FUNCION = "funcion";

    private StaggeredGridLayoutManager mLayoutManager;
    private HorariosAdapter horariosAdapter;
    private Funcion funcion;

    @BindView(R.id.rvHorarios)
    RecyclerView rvHorarios;

    public HorariosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param funcion Parameter 1.
     * @return A new instance of fragment HorariosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HorariosFragment newInstance(Funcion funcion) {
        HorariosFragment fragment = new HorariosFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_FUNCION, funcion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            funcion = getArguments().getParcelable(ARG_FUNCION);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_horarios;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        newAdapter();
    }

    private void newAdapter() {
        horariosAdapter = new HorariosAdapter(getContext(),funcion.getHorarios());
        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        rvHorarios.setLayoutManager(mLayoutManager);
        rvHorarios.setAdapter(horariosAdapter);
    }

    @OnClick(R.id.tvShare)
    public void onClick(View v){
        switch (v.getId()){
            case R.id.tvShare:
                String text = "Mira los horarios para "+ funcion.getMovie().getTitulo() + " de hoy [ "+
                        TextUtils.join(", ",funcion.getHorarios()) + " ] ";
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
        }
    }

}
