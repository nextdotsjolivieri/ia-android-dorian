package com.nextdots.iteractive.ui.modules.listComplejo;

import com.nextdots.iteractive.data.models.Complejo;

import java.util.ArrayList;

public interface ListComplejoView {
    void setComplejosAdapter(ArrayList<Complejo> complejos);
}
