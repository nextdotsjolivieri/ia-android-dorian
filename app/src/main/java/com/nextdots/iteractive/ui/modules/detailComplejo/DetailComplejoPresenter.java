package com.nextdots.iteractive.ui.modules.detailComplejo;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.data.models.Movie;
import com.nextdots.iteractive.data.models.Multimedia;
import com.nextdots.iteractive.ui.modules.detailCity.DetailCityPresenter;
import com.nextdots.iteractive.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DetailComplejoPresenter {
    private final static String TAG = DetailCityPresenter.class.getSimpleName();

    private final Context context;
    private final DetailComplejoView detailComplejoView;

    public DetailComplejoPresenter(Context context, DetailComplejoView detailComplejoView) {
        this.context = context;
        this.detailComplejoView = detailComplejoView;
    }

    public void getMovies(City city, Complejo complejo) {
        String path = Environment.getExternalStorageDirectory() + "/Download/db_"+ city.Id + ".sqlite";
        SQLiteDatabase db = SQLiteDatabase.openDatabase(path, null, 0);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(c.getTime());
        Cursor cursor = db.query(Constant.TABLE_CARTELERA, null,  "IdComplejo='"+complejo.id+"' and Fecha='"+today+"'",
                null, null, null, null);
        ArrayList<Funcion> funciones = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                boolean add_new = true;
                for (Funcion item : funciones){
                    if(item.getId_pelicula() == cursor.getInt(0)){
                        item.addHorario(cursor.getString(5));
                        add_new = false;
                    }
                }

                if(add_new){
                    //NUEVA PELICULA
                    Funcion funcion = new Funcion(
                            cursor.getInt(0),
                            cursor.getString(1),
                            cursor.getInt(2),
                            cursor.getString(3),
                            cursor.getInt(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getInt(8),
                            cursor.getInt(9)
                    );
                    funciones.add(funcion);
                    Cursor cursosMovie = db.query(Constant.TABLE_PELICULA, null,  "Id='"+funcion.getId_pelicula()+"'",
                            null, null, null, null);
                    if (cursosMovie.moveToFirst()) {
                        funcion.setMovie(new Movie(
                                cursosMovie.getInt(0),
                                cursosMovie.getString(1),
                                cursosMovie.getString(2),
                                cursosMovie.getString(3),
                                cursosMovie.getString(4),
                                cursosMovie.getString(5),
                                cursosMovie.getString(6),
                                cursosMovie.getString(7),
                                cursosMovie.getString(8),
                                cursosMovie.getString(9),
                                cursosMovie.getString(10),
                                cursosMovie.getString(11),
                                cursosMovie.getString(12)
                        ));

                        Cursor cursorMultimedia = db.query(Constant.TABLE_MULTIMEDIA, null,
                                "IdPelicula='"+funcion.getId_pelicula()+"' and Tipo='Imagen'",
                                null, null, null, null);
                        ArrayList<Multimedia> multimedias = new ArrayList<>();
                        if (cursorMultimedia.moveToFirst()) {
                            do {
                                multimedias.add(new Multimedia(
                                        cursorMultimedia.getInt(0),
                                        cursorMultimedia.getString(1),
                                        cursorMultimedia.getString(2),
                                        cursorMultimedia.getInt(3)
                                ));
                            } while (cursorMultimedia.moveToNext());
                        }
                        funcion.setMultimedias(multimedias);
                    }else{
                        funcion.setMovie(new Movie());
                        funcion.setMultimedias(new ArrayList<>());
                    }
                }

            } while (cursor.moveToNext());
        }
        detailComplejoView.setupMoviesAdapter(funciones);
    }
}
