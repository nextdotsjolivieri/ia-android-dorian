package com.nextdots.iteractive.ui.modules.detailMovie;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextdots.iteractive.R;
import com.nextdots.iteractive.data.models.City;
import com.nextdots.iteractive.data.models.Complejo;
import com.nextdots.iteractive.data.models.Funcion;
import com.nextdots.iteractive.ui.adapters.DetailMoviePagerAdapter;
import com.nextdots.iteractive.ui.base.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link DetailMovieFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailMovieFragment extends BaseFragment implements DetailMovieView {

    private static final String TAG = DetailMovieFragment.class.getSimpleName();
    private static final String ARG_COMPLEJO = "complejo";
    private static final String ARG_FUNCION = "funcion";
    private static final String ARG_CITY = "city";

    private DetailMoviePagerAdapter moviePagerAdapter;
    private Complejo complejo;
    private Funcion funcion;
    private City city;

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.sliding_tabs)
    TabLayout mViewTabs;


    public DetailMovieFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param city Parameter 1.
     * @param complejo Parameter 2.
     * @param funcion Parameter 3.
     * @return A new instance of fragment DetailMovieFragment.
     */
    public static Fragment newInstance(City city, Complejo complejo, Funcion funcion) {
        DetailMovieFragment fragment = new DetailMovieFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CITY, city);
        args.putParcelable(ARG_COMPLEJO, complejo);
        args.putParcelable(ARG_FUNCION, funcion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            city = getArguments().getParcelable(ARG_CITY);
            complejo = getArguments().getParcelable(ARG_COMPLEJO);
            funcion = getArguments().getParcelable(ARG_FUNCION);
        }
        setHaveToolbar(true);
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_detail_movie;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        moviePagerAdapter = new DetailMoviePagerAdapter(getChildFragmentManager(), getContext(),
                city, complejo, funcion);
        pager.setAdapter(moviePagerAdapter);
        mViewTabs.setupWithViewPager(pager);
        setTitle(funcion.getMovie().getTitulo());
    }


}
