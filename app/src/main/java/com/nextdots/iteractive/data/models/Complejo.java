package com.nextdots.iteractive.data.models;

import android.os.Parcel;
import android.os.Parcelable;


public class Complejo implements Parcelable{
    public int id;
    public String id_complejo_vista;
    public int id_ciudad;
    public String nombre;
    public Double latitud;
    public Double longitud;
    public String telefono;
    public String direccion;
    public boolean es_compra_anticipada;
    public boolean es_reserva_permitida;
    public String url_sitio;

    public final static Parcelable.Creator<Complejo> CREATOR = new Creator<Complejo>() {

        public Complejo createFromParcel(Parcel in) {
            Complejo instance = new Complejo();
            instance.id = in.readInt();
            instance.id_complejo_vista = in.readString();
            instance.id_ciudad = in.readInt();
            instance.nombre = in.readString();
            instance.latitud =  in.readDouble();
            instance.longitud =  in.readDouble();
            instance.telefono = in.readString();
            instance.direccion = in.readString();
            instance.es_compra_anticipada = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            instance.es_reserva_permitida = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            instance.url_sitio = in.readString();
            return instance;
        }

        public Complejo[] newArray(int size) {
            return (new Complejo[size]);
        }

    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(id_complejo_vista);
        dest.writeValue(id_ciudad);
        dest.writeValue(nombre);
        dest.writeValue(latitud);
        dest.writeValue(longitud);
        dest.writeValue(telefono);
        dest.writeValue(direccion);
        dest.writeValue(es_compra_anticipada);
        dest.writeValue(es_reserva_permitida);
        dest.writeValue(url_sitio);
    }

    public int describeContents() {
        return 0;
    }

    public String getAddressShort() {
        int index = direccion.indexOf(",");
        if((index >-1) && (index < 20)){
            return direccion.substring(0,index);
        }else if(direccion.length()> 20) {
            return direccion.substring(0,20) + "...";
        }else{
            return direccion;
        }
    }
}
