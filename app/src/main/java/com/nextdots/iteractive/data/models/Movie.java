package com.nextdots.iteractive.data.models;

/**
 * Created by dars_ on 2/5/2017.
 */

public class Movie {
    private int id;
    private String titulo;
    private String titulo_original;
    private String genero;
    private String clasificacion;
    private String duracion;
    private String director;
    private String actores;
    private String sinopsis;
    private String imagen_cartel;
    private String sello;
    private String sello_estatus;
    private String clave;

    public Movie(int id, String titulo, String titulo_original, String genero, String clasificacion, String duracion, String director, String actores, String sinopsis, String imagen_cartel, String sello, String sello_estatus, String clave) {
        this.id = id;
        this.titulo = titulo;
        this.titulo_original = titulo_original;
        this.genero = genero;
        this.clasificacion = clasificacion;
        this.duracion = duracion;
        this.director = director;
        this.actores = actores;
        this.sinopsis = sinopsis;
        this.imagen_cartel = imagen_cartel;
        this.sello = sello;
        this.sello_estatus = sello_estatus;
        this.clave = clave;
    }

    public Movie() {
        this.titulo = "";
        this.titulo_original = "";
        this.genero = "";
        this.clasificacion = "";
        this.duracion = "";
        this.director = "";
        this.actores = "";
        this.sinopsis = "";
        this.imagen_cartel = "";
        this.sello = "";
        this.sello_estatus = "";
        this.clave = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTitulo_original() {
        return titulo_original;
    }

    public void setTitulo_original(String titulo_original) {
        this.titulo_original = titulo_original;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActores() {
        return actores;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getImagen_cartel() {
        return imagen_cartel;
    }

    public void setImagen_cartel(String imagen_cartel) {
        this.imagen_cartel = imagen_cartel;
    }

    public String getSello() {
        return sello;
    }

    public void setSello(String sello) {
        this.sello = sello;
    }

    public String getSello_estatus() {
        return sello_estatus;
    }

    public void setSello_estatus(String sello_estatus) {
        this.sello_estatus = sello_estatus;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
