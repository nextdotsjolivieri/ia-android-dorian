package com.nextdots.iteractive.data.models;

import android.os.Parcel;
import android.os.Parcelable;


public class Multimedia implements Parcelable{
    private int id_pelicula;
    private String archivo;
    private String tipo;
    private int id;

    public Multimedia(int id_pelicula, String archivo, String tipo, int id) {
        this.id_pelicula = id_pelicula;
        if(archivo != null){
            this.archivo = archivo;
        }else{
            this.archivo = "";
        }
        if(tipo != null){
            this.tipo = tipo;
        }else{
            this.tipo = "";
        }
        this.id = id;
    }

    public Multimedia() {
        id_pelicula = 0;
        id =0;
        archivo = "";
        tipo = "";
    }

    public final static Parcelable.Creator<Multimedia> CREATOR = new Creator<Multimedia>() {

        public Multimedia createFromParcel(Parcel in) {
            Multimedia instance = new Multimedia();
            instance.id_pelicula = in.readInt();
            instance.archivo = in.readString();
            instance.tipo = in.readString();
            instance.id = in.readInt();
            return instance;
        }

        public Multimedia[] newArray(int size) {
            return (new Multimedia[size]);
        }

    };


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id_pelicula);
        dest.writeValue(archivo);
        dest.writeValue(tipo);
        dest.writeValue(id);
    }

    public int describeContents() {
        return 0;
    }

    public int getId_pelicula() {
        return id_pelicula;
    }

    public void setId_pelicula(int id_pelicula) {
        this.id_pelicula = id_pelicula;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
