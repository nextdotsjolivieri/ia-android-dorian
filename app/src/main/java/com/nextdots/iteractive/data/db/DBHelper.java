package com.nextdots.iteractive.data.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "local.db";
    public static final int DB_VERSION = 1;


    //--------TABLA DE PUBLICACIONES-----------------
    public static final String TABLE_CITY="city";
    public static final String C_ESTADO="estado";
    public static final String C_ID="id";
    public static final String C_ID_ESTADO="id_estado";
    public static final String C_ID_PAIS="id_pais";
    public static final String C_LATITUD="latitud";
    public static final String C_LONGITUD="longitud";
    public static final String C_NOMBRE="nombre";
    public static final String C_PAIS="pais";

    public static final int C_ESTADO_INDEX=0;
    public static final int C_ID_INDEX=1;
    public static final int C_ID_ESTADO_INDEX=2;
    public static final int C_ID_PAIS_INDEX=3;
    public static final int C_LATITUD_INDEX=4;
    public static final int C_LONGITUD_INDEX=5;
    public static final int C_NOMBRE_INDEX=6;
    public static final int C_PAIS_INDEX=7;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String  sqlCreate = "create table " + TABLE_CITY + " ( " + C_ESTADO + " text, "
                + C_ID + " integer primary key, "+ C_ID_ESTADO + " integer, "+C_ID_PAIS+
                " integer, "+C_LATITUD+" double, "+C_LONGITUD+" double, "+C_NOMBRE +" text," +
                C_PAIS+" text)";

        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
        db.execSQL("drop table if exists " + TABLE_CITY); // drops the old database

        //Se crea la nueva versión de la tabla
        onCreate(db);
    }
}
